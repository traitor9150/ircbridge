This repository is **archived**. It has been moved to <https://git.sr.ht/~whiisker/ircbridge>


IRCbridge
=====

IRCbridge is a python library for easily creating bridges to IRC from arbitrary web hooks
